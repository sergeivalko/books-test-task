import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridComponent } from './components/grid/grid.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [GridComponent],
  imports: [
    CommonModule,

    MatCheckboxModule,
    MatPaginatorModule,
    MatTableModule,
    MatButtonModule,
  ],
  exports: [GridComponent],
})
export class SharedModule {}
