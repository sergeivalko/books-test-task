import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements AfterViewInit, OnInit {
  @Input() displayedColumns: string[];
  @Input('dataSource') dataSource: MatTableDataSource<any>;
  @Output('selectRows') selectEventEmitter = new EventEmitter<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  public selection = new SelectionModel<any>(true, []);

  ngOnInit(): void {}

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
    this.selectEventEmitter.emit(this.selection.selected);
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.id + 1
    }`;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.dataSource.currentValue != changes.dataSource.previousValue) {
      this.dataSource.paginator = this.paginator;
      this.selection.clear();
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  toggle(row: any) {
    this.selection.toggle(row);
    this.selectEventEmitter.emit(this.selection.selected);
  }
}
