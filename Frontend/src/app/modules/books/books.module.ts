import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BooksRoutingModule } from './books-routing.module';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BooksContainerComponent } from './container/books-container/books-container.component';
import { BooksApi } from './api/books.api';
import { BooksFacade } from './facade/books.facade';
import { BooksState } from './state/books.state';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    BookDetailComponent,
    BooksContainerComponent,
  ],
  imports: [
    CommonModule,
    BooksRoutingModule,

    ReactiveFormsModule,
    FormsModule,

    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    
    SharedModule
  ],
  providers: [BooksApi, BooksFacade, BooksState],
})
export class booksModule {}
