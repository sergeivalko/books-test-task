import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BooksFacade } from '../../facade/books.facade';
import { Book } from '../../models/book';

@Component({
  selector: 'app-books-container',
  templateUrl: './books-container.component.html',
  styleUrls: ['./books-container.component.scss'],
})
export class BooksContainerComponent implements OnInit {
  dataSource$: Observable<MatTableDataSource<Book>>;

  private _selectedRows: Book[];

  constructor(private booksFacade: BooksFacade, private _router: Router) {
    this._selectedRows = [];
  }

  ngOnInit(): void {
    this.dataSource$ = this.booksFacade.get$();
    this.booksFacade.load();
  }

  add() {
    this._router.navigate([`/books/create`]);
  }

  edit() {
    if (this._selectedRows.length !== 1) {
      alert('выберите только одну строку');
      return;
    }

    const row = this._selectedRows[0] as Book;
    this._router.navigate([`/books/${row.id}`]);
  }

  delete() {
    if (this._selectedRows.length == 0) {
      alert('выберите хотя бы одну строку для удаления');
      return;
    }
    const books = this._selectedRows as Book[];
    this.booksFacade.delete(books);
  }

  selectRows(rows: []) {
    this._selectedRows = rows;
  }
}
