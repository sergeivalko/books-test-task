import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../models/book';
import { environment } from 'src/environments/environment';

@Injectable()
export class BooksApi {
  constructor(private _httpClient: HttpClient) {}

  public get(): Observable<Book[]> {
    return this._httpClient.get<Book[]>(`${environment.apiUrl}/api/books`);
  }

  public getById(id: number) {
    return this._httpClient.get<Book[]>(
      `${environment.apiUrl}/api/books/${id}`
    );
  }

  public delete(books: Book[]) {
    const booksId = books.map((x) => x.id);

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        books: booksId,
      },
    };

    return this._httpClient.delete<any>(
      `${environment.apiUrl}/api/books`,
      options
    );
  }

  public update(book: Book, id: number) {
    return this._httpClient.put<Book>(
      `${environment.apiUrl}/api/books/${id}`,
      book
    );
  }

  public create(book: Book) {
    return this._httpClient.post<Book>(
      `${environment.apiUrl}/api/books`,
      book
    );
  }
}
