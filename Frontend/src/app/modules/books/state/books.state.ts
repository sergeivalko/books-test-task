import { Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { Book } from '../models/book';

@Injectable()
export class BooksState {
  private _books$: BehaviorSubject<MatTableDataSource<Book>>;

  constructor() {
    this._books$ = new BehaviorSubject(new MatTableDataSource<Book>([]));
  }

  public get$() {
    return this._books$.asObservable();
  }

  public set(books: Book[]) {
    this._books$.next(new MatTableDataSource(books));
  }

  public findBooks(findBooks: Book[]) {
    const books = this._books$.value.data;
    const booksId = findBooks.map((x) => x.id);
    return books.filter((x) => booksId.includes(x.id));
  }

  public delete(deleteBooks: Book[]) {
    const books = this._books$.value.data;
    const booksId = deleteBooks.map((x) => x.id);
    const newbooks = books.filter((x) => !booksId.includes(x.id));
    this.set(newbooks);
  }

  public addBooks(newbooks: Book[]) {
    const books = this._books$.value.data;
    const resultBooks = books.concat(newbooks);
    this.set(resultBooks);
  }
}
