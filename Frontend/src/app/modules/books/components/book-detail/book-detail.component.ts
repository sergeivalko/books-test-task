import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BooksFacade } from '../../facade/books.facade';
import { Book } from '../../models/book';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss'],
})
export class BookDetailComponent implements OnInit {
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private fb: FormBuilder,
    private _booksFacade: BooksFacade
  ) {}

  private _isCreate = true;
  private _id = 0;

  ngOnInit() {
    this._activatedRoute.params.subscribe((data) => {
      if (data && data.id) {
        this._isCreate = false;
        this._id = data.id;

        this._booksFacade.getById(this._id).subscribe((data) => {
          this.profileForm.patchValue(data);
        });
      }
    });
  }

  profileForm = this.fb.group({
    name: ['', Validators.required],
    year: ['', Validators.required],
    genre: ['', Validators.required],
    author: ['', Validators.required],
  });

  async onSubmit() {
    const book = this.profileForm.value as Book;

    try {
      if (this._isCreate) {
        await this._booksFacade.create(book).toPromise();
      } else {
        await this._booksFacade.update(book, this._id).toPromise();
      }

      this._router.navigate(['./books']);
    } catch (error) {
      alert('ошибка проверьте консоль');
      console.error(error);
    }
  }
}
