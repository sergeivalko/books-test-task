import { TestBed } from '@angular/core/testing';

import { BooksFacade } from './books.facade';

describe('booksFacade', () => {
  let service: BooksFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BooksFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
