import { Injectable } from '@angular/core';
import { BooksApi } from '../api/books.api';
import { BooksState } from '../state/books.state';
import { Book } from '../models/book';
@Injectable()
export class BooksFacade {
  constructor(private _api: BooksApi, private _state: BooksState) {}

  public load() {
    return this._api.get().subscribe((books) => {
      this._state.set(books);
    });
  }

  public get$() {
    return this._state.get$();
  }

  public getById(id: number) {
    return this._api.getById(id);
  }

  public create(book: Book) {
    return this._api.create(book);
  }

  public update(book: Book, id: number) {
    return this._api.update(book, id);
  }

  public delete(books: Book[]) {
    const removedBooks = this._state.findBooks(books);
    this._state.delete(books);

    this._api.delete(books).subscribe(
      () => {
        alert('Успешно удален');
        this.load();
      },
      (error) => {
        this._state.addBooks(removedBooks);
        alert('Ошибка удаления. Смотри консоль.');
        console.error(error);
      }
    );
  }
}
