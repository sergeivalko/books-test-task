import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BooksContainerComponent } from './container/books-container/books-container.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: BooksContainerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: BookDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':id',
    component: BookDetailComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BooksRoutingModule {}
