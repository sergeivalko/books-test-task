import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { TokenDto } from '../models/token-dto.model';

@Injectable({ providedIn: 'root' })
export class AuthState {
  private _tokenDto$: BehaviorSubject<TokenDto>;
  private _isAuth$: BehaviorSubject<boolean>;
  private _errors$: Subject<string>;
  constructor() {
    this._tokenDto$ = new BehaviorSubject<TokenDto>(null);
    this._isAuth$ = new BehaviorSubject<boolean>(false);
    this._errors$ = new Subject<string>();
  }

  public setToken(tokenDto: TokenDto) {
    this._tokenDto$.next(tokenDto);
  }

  public getToken() {
    return this._tokenDto$.value;
  }

  public isAuth() {
    return this._isAuth$.value;
  }

  public authentication() {
    return this._isAuth$.next(true);
  }

  public logout() {
    this._isAuth$.next(false);
    this._tokenDto$.next(null);
  }

  public getErrors$() {
    return this._errors$.asObservable();
  }

  public setErrors(error: string) {
    this._errors$.next(error);
  }
}
