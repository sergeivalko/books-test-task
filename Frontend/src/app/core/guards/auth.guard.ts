import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthState } from '../state/auth.state';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private _router: Router, private _state: AuthState) {}

  public async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    if (!this._state.isAuth()) {
      this._state.logout();
      this._router.navigate(['/login'], {
        queryParams: { returnUrl: state.url },
      });
    }

    return true;
  }
}
