import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthState } from '../state/auth.state';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private _state: AuthState) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const auth = this._state.getToken();
    if (auth && auth.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${auth.token}`,
        },
      });
    }

    return next.handle(request);
  }
}
