import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Credentials } from '../models/credentionals.model';
import { TokenDto } from '../models/token-dto.model';

@Injectable({ providedIn: 'root' })
export class AuthApi {
  constructor(private _httpClient: HttpClient) {}

  public authentication(credentionals: Credentials) {
    const queryHeaders = new HttpHeaders({
      Authorization:
        'Basic ' +
        btoa(
          unescape(
            encodeURIComponent(
              `${credentionals.login}:${credentionals.password}`
            )
          )
        ),
      Accept: 'application/json',
    });
    return this._httpClient.post<TokenDto>(
      `${environment.apiUrl}/api/auth/token`,
      null,
      { headers: queryHeaders }
    );
  }
}
