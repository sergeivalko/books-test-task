import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthApi } from '../api/auth.api';
import { Credentials } from '../models/credentionals.model';
import { AuthState } from '../state/auth.state';

@Injectable({
  providedIn: 'root',
})
export class AuthFacade {
  constructor(
    private _api: AuthApi,
    private _state: AuthState,
    private _router: Router
  ) {}

  public getErrors$() {
    return this._state.getErrors$();
  }

  public authentication(credentionals: Credentials) {
    this._api.authentication(credentionals).subscribe(
      (tokenDto) => {
        if (tokenDto && tokenDto.token) {
          this._state.setToken(tokenDto);
          this._state.authentication();
          this._router.navigateByUrl('/books', {
            skipLocationChange: false,
          });
        }
      },
      (error) => {
        this._state.logout();
        this._state.setErrors(error.error);
      }
    );
  }
}
