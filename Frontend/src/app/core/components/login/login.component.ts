import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthFacade } from '../../facade/auth.facade';
import { Credentials } from '../../models/credentionals.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private fb: FormBuilder, private authFacade: AuthFacade) {}

  ngOnInit() {
    this.authFacade.getErrors$().subscribe((error) => {
      alert('произошла ошибка, посмотрите консоль!');
      console.warn(error);
    });
  }

  loginForm = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required],
  });

  onSubmit() {
    const credentionals = this.loginForm.value as Credentials;
    this.authFacade.authentication(credentionals);
  }
}
