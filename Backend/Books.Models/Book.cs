﻿namespace Books.Models
{
    public class Book
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Genre { get; set; }
        public string Author { get; set; }
    }
}