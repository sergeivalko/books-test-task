﻿using System;
using System.Security.Cryptography;
using System.Text;
using Books.Common.Dto;

namespace Books.Common.Helpers
{
    public static class AuthHelper
    {
        public static AuthDto ParseLoginAndPassword(string header)
        {
            var credValue = header.Substring("Basic ".Length).Trim();
            var userNameAndPass = Encoding.UTF8.GetString(Convert.FromBase64String(credValue)); //admin:pass
            var userNamePwd = userNameAndPass.Split(':');

            return new AuthDto
            {
                Login = userNamePwd[0],
                Password = userNamePwd[1]
            };
        }

        public static string EncodePassword(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(Encoding.ASCII.GetBytes(text));

            var result = md5.Hash;
            var strBuilder = new StringBuilder();
            foreach (var t in result)
            {
                strBuilder.Append(t.ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }
}