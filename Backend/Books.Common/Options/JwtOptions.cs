﻿namespace Books.Common.Options
{
    public class JwtOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public long ExpiresSeconds { get; set; }
        public string CryptoKey { get; set; }
    }
}