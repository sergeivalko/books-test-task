﻿using AutoMapper;
using Books.Common.Dto;
using Books.Common.Helpers;
using Books.Models;

namespace Books.Common.Profiles
{
    public class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<AuthDto, User>()
                .ForMember(x => x.Password, y => y.MapFrom(x => AuthHelper.EncodePassword(x.Password)));
        }
    }
}