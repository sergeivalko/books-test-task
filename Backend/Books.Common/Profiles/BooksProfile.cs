﻿using AutoMapper;
using Books.Common.Dto;
using Books.Models;

namespace Books.Common.Profiles
{
    public class BooksProfile : Profile
    {
        public BooksProfile()
        {
            CreateMap<Book, BookDto>()
                .ReverseMap();
        }
    }
}