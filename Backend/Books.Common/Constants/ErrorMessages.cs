﻿namespace Books.Common.Constants
{
    public static class ErrorMessages
    {
        public const string FieldEmptyOrWrongFormat = "Поле не заполнено или имеет неверный формат";
        public const string NotFound = "Запись не найдена";
    }
}