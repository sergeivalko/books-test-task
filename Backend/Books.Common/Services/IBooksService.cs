﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Books.Common.Dto;

namespace Books.Common.Services
{
    public interface IBooksService
    {
        Task<List<BookDto>> Get(CancellationToken cancellationToken = default);
        Task<BookDto> GetById(long id, CancellationToken cancellationToken = default);
        Task<BookDto> Create(BookDto bookDto, CancellationToken cancellationToken = default);
        Task<BookDto> Update(BookDto bookDto, long id, CancellationToken cancellationToken = default);
        Task Delete(RequestBooksDeleteDto booksDeleteDto, CancellationToken cancellationToken = default);
    }
}