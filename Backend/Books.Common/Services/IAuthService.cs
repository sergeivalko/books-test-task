﻿using System.Threading;
using System.Threading.Tasks;
using Books.Common.Dto;

namespace Books.Common.Services
{
    public interface IAuthService
    {
        Task<TokenDto> AuthenticateUser(AuthDto authDto, CancellationToken cancellationToken);
    }
}