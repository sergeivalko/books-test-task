﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Books.Models;

namespace Books.Common.Repositories
{
    public interface IBooksRepository
    {
        Task<List<Book>> Get(CancellationToken cancellationToken = default);
        Task<List<Book>> Get(Expression<Func<Book, bool>> expression, CancellationToken cancellationToken = default);
        Task<Book> GetById(long id, CancellationToken cancellationToken = default);
        Task<Book> Add(Book book, CancellationToken cancellationToken = default);
        Task<Book> Update(Book book, CancellationToken cancellationToken = default);
        Task<Book> Save(Book book, CancellationToken cancellationToken = default);
        Task Delete(IEnumerable<Book> books, CancellationToken cancellationToken = default);
    }
}