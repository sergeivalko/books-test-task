﻿using System.Threading;
using System.Threading.Tasks;
using Books.Models;

namespace Books.Common.Repositories
{
    public interface IUsersRepository
    {
        Task<User> Authentication(User user, CancellationToken cancellationToken);
    }
}