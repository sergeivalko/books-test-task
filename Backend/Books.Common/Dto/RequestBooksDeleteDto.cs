﻿using System.Collections.Generic;

namespace Books.Common.Dto
{
    public class RequestBooksDeleteDto
    {
        public List<long> Books { get; set; }
    }
}