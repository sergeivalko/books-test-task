﻿using Books.Common.Constants;
using Books.Common.Dto.Validators.Base;
using FluentValidation;

namespace Books.Common.Dto.Validators
{
    public class RequestBooksDeleteDtoValidator: BaseAbstractValidator<RequestBooksDeleteDto>
    {
        public RequestBooksDeleteDtoValidator()
        {
            RuleFor(x=>x.Books)
                .NotEmpty()
                .WithMessage(ErrorMessages.FieldEmptyOrWrongFormat);
        }
    }
}