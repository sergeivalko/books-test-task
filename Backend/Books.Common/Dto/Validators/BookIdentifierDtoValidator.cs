﻿using Books.Common.Constants;
using Books.Common.Dto.Validators.Base;
using FluentValidation;

namespace Books.Common.Dto.Validators
{
    public class BookIdentifierDtoValidator : BaseAbstractValidator<BookIdentifierDto>
    {
        public BookIdentifierDtoValidator()
        {
            RuleFor(x=>x.BookId)
                .GreaterThan(0)
                .WithMessage(ErrorMessages.FieldEmptyOrWrongFormat);
        }
    }
}