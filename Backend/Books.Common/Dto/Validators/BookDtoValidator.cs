﻿using System;
using Books.Common.Constants;
using Books.Common.Dto.Validators.Base;
using FluentValidation;

namespace Books.Common.Dto.Validators
{
    public class BookDtoValidator : BaseAbstractValidator<BookDto>
    {
        public BookDtoValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(ErrorMessages.FieldEmptyOrWrongFormat);

            RuleFor(x => x.Author)
                .NotEmpty()
                .WithMessage(ErrorMessages.FieldEmptyOrWrongFormat);
            
            RuleFor(x => x.Genre)
                .NotEmpty()
                .WithMessage(ErrorMessages.FieldEmptyOrWrongFormat);
            
            RuleFor(x => x.Year)
                .InclusiveBetween(1600, DateTime.Now.Year)
                .WithMessage(ErrorMessages.FieldEmptyOrWrongFormat);
        }
    }
}