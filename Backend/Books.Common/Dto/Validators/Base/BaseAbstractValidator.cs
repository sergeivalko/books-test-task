﻿using System.Globalization;
using FluentValidation;

namespace Books.Common.Dto.Validators.Base
{
    public abstract class BaseAbstractValidator<T> : AbstractValidator<T>
    {
        const string CULTURE_RU = "ru";

        protected BaseAbstractValidator()
        {
            ValidatorOptions.LanguageManager.Culture = new CultureInfo(CULTURE_RU);
            ValidatorOptions.PropertyNameResolver = CamelCasePropertyNameResolver.ResolvePropertyName;
        }
    }
}