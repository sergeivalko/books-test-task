﻿namespace Books.Common.Dto
{
    public class BookDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Genre { get; set; }
        public string Author { get; set; }
    }
}