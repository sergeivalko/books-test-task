﻿namespace Books.Common.Dto
{
    public class TokenDto
    {
        public string Token { get; set; }
    }
}