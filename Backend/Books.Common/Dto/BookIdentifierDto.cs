﻿namespace Books.Common.Dto
{
    public class BookIdentifierDto
    {
        public long BookId { get; set; }
    }
}