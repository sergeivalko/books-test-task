﻿using Books.Common.Dto;
using Books.Common.Dto.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace Books.Common
{
    public static class ConfigureServices
    {
        public static IServiceCollection ConfigureCommon(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IValidator<BookDto>, BookDtoValidator>();
            serviceCollection.AddTransient<IValidator<BookIdentifierDto>, BookIdentifierDtoValidator>();
            serviceCollection.AddTransient<IValidator<RequestBooksDeleteDto>, RequestBooksDeleteDtoValidator>();
            return serviceCollection;
        }
    }
}
