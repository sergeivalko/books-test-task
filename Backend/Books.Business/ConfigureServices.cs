﻿using Books.Business.Services;
using Books.Common.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Books.Business
{
    public static class ConfigureServices
    {
        public static IServiceCollection ConfigureBusiness(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IBooksService, BooksService>();
            serviceCollection.AddScoped<IAuthService, AuthService>();
            return serviceCollection;
        }
    }
}