﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Books.Common.Constants;
using Books.Common.Dto;
using Books.Common.Exceptions;
using Books.Common.Options;
using Books.Common.Repositories;
using Books.Common.Services;
using Books.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Books.Business.Services
{
    public class AuthService : IAuthService
    {
        private readonly JwtOptions _jwtOptions;
        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public AuthService(IOptions<JwtOptions> options, IUsersRepository usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
            _jwtOptions = options.Value;
        }

        private TokenDto CreateTokenPair(IEnumerable<Claim> claims)
        {
            var token = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                expires: DateTime.Now.AddSeconds(_jwtOptions.ExpiresSeconds),
                claims: claims,
                signingCredentials: GetSignInCred(_jwtOptions.CryptoKey)
            );

            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
            return new TokenDto
            {
                Token = tokenString
            };
        }

        private SigningCredentials GetSignInCred(string tokenCryptionKey)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenCryptionKey));
            var signInCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            return signInCred;
        }

        private IEnumerable<Claim> GetDefaultClaims(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.FullName),
                new Claim("UserId", user.Id.ToString())
            };
            return claims;
        }

        public async Task<TokenDto> AuthenticateUser(AuthDto authDto, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<User>(authDto);
            user = await _usersRepository.Authentication(user, cancellationToken);

            if (user == null)
            {
                throw new UserNotFoundException(ErrorMessages.NotFound);
            }
            
            var defaultClaims = GetDefaultClaims(user);
            var tokenPairDto = CreateTokenPair(defaultClaims);
            return tokenPairDto;
        }
    }
}