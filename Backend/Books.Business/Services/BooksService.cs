﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Books.Common.Constants;
using Books.Common.Dto;
using Books.Common.Repositories;
using Books.Common.Services;
using Books.Models;

namespace Books.Business.Services
{
    public class BooksService : IBooksService
    {
        private readonly IBooksRepository _booksRepository;
        private readonly IMapper _mapper;

        public BooksService(IBooksRepository booksRepository, IMapper mapper)
        {
            _booksRepository = booksRepository;
            _mapper = mapper;
        }

        public async Task<List<BookDto>> Get(CancellationToken cancellationToken = default)
        {
            var books = await _booksRepository.Get(cancellationToken);
            var booksDto = _mapper.Map<List<BookDto>>(books);
            return booksDto;
        }

        public async Task<BookDto> Create(BookDto bookDto, CancellationToken cancellationToken = default)
        {
            var book = _mapper.Map<Book>(bookDto);
            book = await _booksRepository.Save(book, cancellationToken);
            bookDto = _mapper.Map<BookDto>(book);
            return bookDto;
        }

        public async Task<BookDto> Update(BookDto bookDto, long id, CancellationToken cancellationToken = default)
        {
            var existBook = await _booksRepository.GetById(id, cancellationToken);

            if (existBook == null)
            {
                throw new Exception(ErrorMessages.NotFound);
            }

            bookDto.Id = id;
            var book = _mapper.Map<Book>(bookDto);
            book = await _booksRepository.Save(book, cancellationToken);
            bookDto = _mapper.Map<BookDto>(book);
            return bookDto;
        }

        public async Task Delete(RequestBooksDeleteDto booksDeleteDto, CancellationToken cancellationToken = default)
        {
            var books = await _booksRepository.Get(x => booksDeleteDto.Books.Contains(x.Id), cancellationToken);
            await _booksRepository.Delete(books, cancellationToken);
        }

        public async Task<BookDto> GetById(long id, CancellationToken cancellationToken = default)
        {
            var book = await _booksRepository.GetById(id, cancellationToken);

            if (book == null)
            {
                throw new Exception(ErrorMessages.NotFound);
            }

            var bookDto = _mapper.Map<BookDto>(book);
            return bookDto;
        }
    }
}