﻿using Books.Common.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Books.API.Extensions
{
    public static class OptionsConfigure
    {
        public static IServiceCollection AddOptionsSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions<JwtOptions>()
                .Bind(configuration.GetSection("Authentication"));

            return services;
        } 
    }
}