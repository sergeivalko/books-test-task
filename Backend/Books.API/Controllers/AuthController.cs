﻿using System;
using System.Threading.Tasks;
using Books.Common.Helpers;
using Books.Common.Services;
using Microsoft.AspNetCore.Mvc;

namespace Books.API.Controllers
{
    [ApiController]
    [Route("/api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        private bool IsBasicAuthorization(out string header)
        {
            header = Request.Headers["Authorization"];
            return header != null && header.StartsWith("Basic");
        }

        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> Token()
        {
            if (!IsBasicAuthorization(out var header))
            {
                throw new Exception("BasicAuthorization error!");
            }

            var authDto = AuthHelper.ParseLoginAndPassword(header);
            var tokenPair = await _authService.AuthenticateUser(authDto, HttpContext.RequestAborted);
            return Ok(tokenPair);
        }
    }
}