﻿using System.Threading.Tasks;
using Books.Common.Dto;
using Books.Common.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Books.API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("/api/books")]
    public class BooksController : ControllerBase
    {
        private readonly IBooksService _booksService;

        public BooksController(IBooksService booksService)
        {
            _booksService = booksService;
        }

        [HttpGet]
        public async Task<IActionResult> GetBooks()
        {
            var books = await _booksService.Get(HttpContext.RequestAborted);
            return Ok(books);
        }

        [HttpGet]
        [Route("{bookId}")]
        public async Task<IActionResult> GetBook([FromRoute] BookIdentifierDto bookIdentifierDto)
        {
            var book = await _booksService.GetById(bookIdentifierDto.BookId, HttpContext.RequestAborted);
            return Ok(book);
        }

        [HttpPost]
        public async Task<IActionResult> CreateBook([FromBody] BookDto bookDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createdBook = await _booksService.Create(bookDto, HttpContext.RequestAborted);
            return Ok(createdBook);
        }

        [HttpPut]
        [Route("{bookId}")]
        public async Task<IActionResult> UpdateBook([FromRoute] BookIdentifierDto bokBookIdentifierDto,
            [FromBody] BookDto bookDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updatedBooks = await _booksService.Update(bookDto, bokBookIdentifierDto.BookId, HttpContext.RequestAborted);
            return Ok(updatedBooks);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteBooks([FromBody] RequestBooksDeleteDto deleteBooksDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _booksService.Delete(deleteBooksDto);
            return NoContent();
        }
    }
}