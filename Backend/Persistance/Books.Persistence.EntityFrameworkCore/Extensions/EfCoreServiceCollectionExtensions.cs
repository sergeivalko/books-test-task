﻿using System;
using Books.Common.Repositories;
using Books.Persistence.EntityFrameworkCore.Contexts;
using Books.Persistence.EntityFrameworkCore.HostedServices;
using Books.Persistence.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Books.Persistence.EntityFrameworkCore.Extensions
{
    public static class EfCoreServiceCollectionExtensions
    {
        public static IServiceCollection AddEntityFrameworkStores<TContext>(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> configureOptions) where TContext : BooksContext
        {
            return services
                .AddEntityFrameworkCoreProvider<TContext>(configureOptions)
                .AddBooksRepositories();
        }

        private static IServiceCollection AddEntityFrameworkCoreProvider<TContext>(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> configureOptions) where TContext : BooksContext
        {
            services.AddDbContext<BooksContext, TContext>(configureOptions);
            services.AddHostedService<RunMigrations>();
            return services;
        }

        private static IServiceCollection AddBooksRepositories(
            this IServiceCollection services)
        {
            services.AddScoped<IBooksRepository, BooksRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            return services;
        }
    }
}