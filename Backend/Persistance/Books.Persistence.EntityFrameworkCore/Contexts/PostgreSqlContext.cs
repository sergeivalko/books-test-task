﻿using Microsoft.EntityFrameworkCore;

namespace Books.Persistence.EntityFrameworkCore.Contexts
{
    public class PostgreSqlContext : BooksContext
    {
        public PostgreSqlContext(DbContextOptions options) : base(options)
        {
        }
    }
}