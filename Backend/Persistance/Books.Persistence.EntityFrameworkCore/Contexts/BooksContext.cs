﻿using Books.Models;
using Microsoft.EntityFrameworkCore;

namespace Books.Persistence.EntityFrameworkCore.Contexts
{
    public class BooksContext : DbContext
    {
        public BooksContext(DbContextOptions<BooksContext> options) : base(options)
        {
        }

        /// <summary>
        /// Constructor to initialise a new <see cref="ElsaContext"/> that will be ignored by Dependency Injection.
        /// Use this constructor when creating derived classes, i.e. for each database provider implementation.
        /// </summary>
        /// <param name="options"></param>
        /// <remarks>Protected for following reason: https://github.com/aspnet/EntityFramework.Docs/issues/594</remarks>
        protected BooksContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().HasData(new User { Id = 1, Login = "admin", Password = "21232f297a57a5a743894a0e4a801fc3", FullName = "Иванов Иван Иванович"});
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
    }
}