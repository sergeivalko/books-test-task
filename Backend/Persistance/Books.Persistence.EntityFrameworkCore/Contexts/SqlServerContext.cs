﻿using Microsoft.EntityFrameworkCore;

namespace Books.Persistence.EntityFrameworkCore.Contexts
{
    public class SqlServerContext : BooksContext
    {
        public SqlServerContext(DbContextOptions options) : base(options)
        {
        }
    }
}