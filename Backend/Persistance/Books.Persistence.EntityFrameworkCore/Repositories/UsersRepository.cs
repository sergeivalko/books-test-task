﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Books.Common.Repositories;
using Books.Models;
using Books.Persistence.EntityFrameworkCore.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Books.Persistence.EntityFrameworkCore.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly BooksContext _dbContext;

        public UsersRepository(BooksContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public Task<User> Authentication(User user, CancellationToken cancellationToken)
        {
            var query = _dbContext.Users.Where(x => x.Login == user.Login && x.Password == user.Password);
            return query.FirstOrDefaultAsync(cancellationToken);
        }
    }
}