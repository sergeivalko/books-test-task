﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Books.Common.Repositories;
using Books.Models;
using Books.Persistence.EntityFrameworkCore.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Books.Persistence.EntityFrameworkCore.Repositories
{
    public class BooksRepository : IBooksRepository
    {
        private readonly BooksContext _dbContext;

        public BooksRepository(BooksContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Book>> Get(CancellationToken cancellationToken = default)
        {
            var query = _dbContext.Books.AsNoTracking();
            var entities = await query.ToListAsync(cancellationToken);
            return entities;
        }

        public async Task<List<Book>> Get(Expression<Func<Book, bool>> expression, CancellationToken cancellationToken = default)
        {
            var query = _dbContext.Books.AsNoTracking().Where(expression);
            var entities = await query.ToListAsync(cancellationToken);
            return entities;
        }

        public async Task<Book> Save(Book book, CancellationToken cancellationToken)
        {
            var existingEntity = await GetById(book.Id, cancellationToken);

            if (existingEntity == null)
                return await Add(book, cancellationToken);

            return await Update(book, cancellationToken);
        }

        public async Task Delete(IEnumerable<Book> books, CancellationToken cancellationToken = default)
        {
            _dbContext.Books.RemoveRange(books);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<Book> Add(Book book, CancellationToken cancellationToken = default)
        {
            await _dbContext.Books.AddAsync(book, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return book;
        }

        public async Task<Book> Update(Book book, CancellationToken cancellationToken = default)
        {
            _dbContext.Books.Update(book);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return book;
        }

        public async Task<Book> GetById(long id, CancellationToken cancellationToken = default)
        {
            var entity = await _dbContext.Books.Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync(cancellationToken);
            return entity;
        }
    }
}