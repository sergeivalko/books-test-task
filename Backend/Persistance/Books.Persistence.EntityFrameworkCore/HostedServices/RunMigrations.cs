﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Books.Persistence.EntityFrameworkCore.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Books.Persistence.EntityFrameworkCore.HostedServices
{
    public class RunMigrations : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        public RunMigrations(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using var scope = _serviceProvider.CreateScope();
            var elsaContext = scope.ServiceProvider.GetRequiredService<BooksContext>();
            await elsaContext.Database.MigrateAsync(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}